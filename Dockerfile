FROM node:10.15.3 AS builder
USER node
WORKDIR /home/node
COPY --chown=node:node package*.json ./
RUN ["npm", "i"]
COPY --chown=node:node . .
RUN ["npm", "run", "prestart:prod"]

FROM builder as tester
USER node
WORKDIR /home/node
COPY --chown=node:node --from=builder /home/node .
RUN ["npm", "run", "test"]

FROM builder as linter
USER node
WORKDIR /home/node
COPY --chown=node:node --from=builder /home/node .
RUN ["npm", "run", "lint"]

FROM node:10.15.3-alpine
USER node
WORKDIR /home/node
COPY --chown=node:node --from=builder /home/node .
ENTRYPOINT ["node", "dist/main.js"]