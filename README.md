# Kubernetes CI / CD example
> Deploying a NodeJS microservice

To play around you can fork this repo and add all secrets you find in this repo to your github repository fork

## Gitlab CI
Gitlab manages it's pipelines through a `.gitlab-ci.yml` file. The moment you push your repo with this file included, a pipeline that has no branching restrictions will directly be triggered. 

For all the pipeline configuration options, refer to: https://docs.gitlab.com/ee/ci/yaml/

## Important Kubernetes preparations

### Helm & tiller
Install tiller in your cluster with the command `helm init`. By folowing proper RBAC policies it is better to create a service account for Tiller to reduce it's access in your cluster. In this example, I assume you have tiller installed with an RBAC account that permits tiller to only make changes in the provided namespace.

### RBAC
Create a service account for the CI and store the token in gitlab's environment variable section (through the web interface at their CI settings page)

``` yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: ci
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: ci
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: ci
  namespace: kube-system
```

### Pull secrets
For private registries, you'll need image pull secrets in kubernetes

``` bash
function create_registry_secret_if_not_yet() {
  kubectl create secret docker-registry \
   -n $ENVIRONMENT dockerhub-secret \
   --docker-server=$REGISTRY_DOMAIN \
   --docker-username=$REGISTRY_USER \
   --docker-password=$REGISTRY_PASSWORD \
   --docker-email=$REGISTRY_EMAIL \
   --dry-run=true -o yaml | kubectl apply -f -    # Create if none exists, otherwise ignore
}
```

To ensure your deployment will use these secrets, add the following yaml keys and values to your deployment file (as you can see in the provided example)

``` yaml
imagePullSecrets:
- name: dockerhub-secret
```

## Branching flow

### Branches
A very simple example just to give an idea of how gitlab manages your branching flow

* Static branches
  * DEV
  * MASTER
* Dynamic branches
  * feature/INT-01
  * release/v0.1.0
  * hotfix/INT-03
  * support/v1

### Pipeline flow according to branches
* Pushing anything will trigger a build and test
* pushing a branch with a name starting `release` triggers a buil, test and staging deploy
* merging release into master triggers a production deploy