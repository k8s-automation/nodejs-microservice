#!/usr/bin/env sh

function authenticate() {
  kubectl config set-cluster azure-free --server="$KUBE_URL" --insecure-skip-tls-verify=true
  kubectl config set-credentials ci --token="$KUBE_USER_TOKEN"
  kubectl config set-context default --cluster=azure-free --user=ci
  kubectl config use-context default
}

function apply_manifests() {
  printf "apiVersion: v1\nkind: Secret\n$(kubectl create secret docker-registry dockerhub-secret --docker-server=$CI_REGISTRY --docker-username=$CI_REGISTRY_USER --docker-password=$CI_REGISTRY_PASSWORD --docker-email=$CI_REGISTRY_EMAIL -o yaml --dry-run)"
  
  sed -i -e "s/<APP_NAME>/$APP_NAME/g" -e "s/<NAMESPACE>/$ENVIRONMENT/g" -e "s/<TAG>/$TAG/g" ops/k8s/vanilla/deployment.yaml
  sed -i -e "s/<APP_NAME>/${APP_NAME}/g" -e "s/<NAMESPACE>/${ENVIRONMENT}/g"  ops/k8s/vanilla/service.yaml
  sed -i -e "s/<APP_NAME>/${APP_NAME}/g" -e "s/<NAMESPACE>/${ENVIRONMENT}/g"  ops/k8s/vanilla/ingress.yaml
  
  kubectl apply -f ops/k8s/vanilla/deployment.yaml
  kubectl apply -f ops/k8s/vanilla/service.yaml
  kubectl apply -f ops/k8s/vanilla/ingress.yaml
  
  kubectl -n $ENVIRONMENT rollout status deployment $APP_NAME  
}

authenticate
apply_manifests