#!/usr/bin/env sh

function authenticate_k8s() {
  kubectl config set-cluster azure-free --server="$K8S_URL" --insecure-skip-tls-verify=true
  kubectl config set-credentials ci --token="$K8S_USER_TOKEN"
  kubectl config set-context default --cluster=azure-free --user=ci
  kubectl config use-context default
}

function apply_manifests() {
  helm upgrade \
    --install \
    $APP_NAME \
    ./ops/k8s/$APP_NAME \
    --tiller-namespace=$ENVIRONMENT \
    --namespace=$ENVIRONMENT \
    --set environment=$ENVIRONMENT \
    --set registryRepo=$REGISTRY_REPO \
    --set deployment.imageTag=$TAG \
    --wait
}

create_registry_secret_if_not_yet
authenticate_k8s
apply_manifests