#!/usr/bin/env sh

function authenticate() {
  kubectl config set-cluster azure-free --server="$K8S_URL" --insecure-skip-tls-verify=true
  kubectl config set-credentials ci --token="$K8S_USER_TOKEN"
  kubectl config set-context default --cluster=azure-free --user=ci
  kubectl config use-context default
}

function apply_manifests() {
  helm upgrade --install \
    --tiller-namespace=$ENVIRONMENT \
    $APP_NAME \
    ./ops/k8s/$APP_NAME \
    --namespace=$ENVIRONMENT \
    --set registryRepo=$REGISTRY_REPO \
    --set imageTag=$TAG \
    --wait
}

authenticate
apply_manifests